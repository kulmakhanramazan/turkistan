// Email: avtosalon098@gmail.com
// Password: Ramazan@123
import React from 'react';

export default async function(to, subject, body) {
  return await fetch('https://ramazan.io/api/sendmail', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      to: to,
      subject: subject,
      body: body,
    }),
  });
}
