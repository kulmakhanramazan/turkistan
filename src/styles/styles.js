import * as React from 'react';
import {StyleSheet, Dimensions} from 'react-native';

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
export const successColor = '#25D993';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EBEBEF',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  header: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  headerText: {
    fontSize: 25,
    fontWeight: '700',
  },
  content: {
    backgroundColor: '#EBEBEF',
    width: '100%',
  },
  carMenuItem: {
    width: deviceWidth - 10 * 2,
    height: 170,
    overflow: 'hidden',
    borderRadius: 20,
    margin: 10,
    position: 'relative',
  },
  carMenuItemImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  carMenuItemBody: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  carMenuItemText: {
    fontSize: 25,
    color: 'white',
  },
  carListItem: {
    width: deviceWidth - 10 * 2,
    height: 250,
    overflow: 'hidden',
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
  },
  carsListImage: {
    width: '100%',
    height: 220,
  },
  carsListBody: {
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  carsListName: {
    fontSize: 16,
  },
  carsListPrice: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  carouselItem: {
    backgroundColor: '#e6e6e6',
  },
  carouselImage: {
    width: '100%',
    height: 300,
  },
  carouselTextBody: {
    paddingHorizontal: 10,
  },
  detailsContainer: {
    backgroundColor: 'white',
  },
  detailsNameContainer: {
    paddingBottom: 15,
    marginBottom: 10,
  },
  detailsName: {
    fontSize: 35,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  detailsPrice: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  detailsInfo: {
    backgroundColor: 'white',
    borderRadius: 15,
    width: '100%',
    elevation: 7,
    marginVertical: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: 10,
  },
  detailsInfoType: {
    width: deviceWidth / 2 - 20,
    marginVertical: 5,
  },
  detailsInfoTypeTxt: {
    color: 'gray',
    fontSize: 16,
  },
  detailsInfoText: {
    width: deviceWidth / 2 - 20,
  },
  detailsInfoTextTxt: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  detailsComment: {
    fontSize: 16,
  },
  serviceImageContainer: {
    width: deviceWidth,
    height: 250,
    position: 'relative',
  },
  serviceImage: {
    width: '100%',
    height: '100%',
  },
  serviceImageText: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  serviceImageTextTxt: {
    textAlign: 'center',
    color: 'white',
    fontSize: 30,
  },
  serviceStepContainer: {
    backgroundColor: 'black',
    padding: 10,
  },
  serviceStepItem: {
    flexDirection: 'row',
  },
  serviceStepItemDown: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 7,
  },
  serviceStepItemText: {
    color: 'white',
    marginLeft: 10,
    fontSize: 16,
    marginTop: 5,
  },
  formSecondaryText: {
    color: 'gray',
    fontSize: 16,
    marginHorizontal: 10,
    marginTop: 15,
  },
  formSelect: {
    width: '100%',
    marginHorizontal: 5,
  },
  contactTitle: {
    color: '#2AABE2',
    fontSize: 27,
    marginBottom: 10,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  contactContainer: {
    padding: 10,
    backgroundColor: 'white',
    minHeight: deviceHeight - 300,
  },
  contactIconWithText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  contactText: {
    fontSize: 16,
  },
  contactIcon: {
    marginRight: 10,
  },
  formDateTime: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 11,
    marginTop: 25,
  },
  formContainer: {
    paddingHorizontal: 10,
  },
  textInput: {
    borderColor: 'gray',
    borderWidth: 0.5,
    paddingLeft: 15,
    paddingVertical: 5,
    marginVertical: 15,
    borderRadius: 15,
    fontSize: 16,
  },
  sendButtonText: {
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 16,
  },
  sendButton: {
    backgroundColor: '#2AABE2',
    overflow: 'hidden',
    marginVertical: 15,
    padding: 12,
    borderRadius: 15,
  },
  infoTitleContainer: {
    position: 'relative',
    width: '100%',
    height: 200,
  },
  infoImage: {
    width: '100%',
    height: '100%',
  },
  infoTitleWrapper: {
    position: 'absolute',
    width: '100%',
    bottom: 10,
    left: 10,
  },
  infoTitle: {
    color: '#ffffff',
    fontSize: 40,
    fontWeight: 'bold',
  },
  infoSubtitle: {
    color: '#ffffff',
    fontSize: 20,
  },
  infoBody: {
    paddingHorizontal: 10,
  },
  infoHeader: {
    borderBottomColor: successColor,
    borderBottomWidth: 1,
    alignSelf: 'flex-start',
    paddingVertical: 5,
    marginVertical: 10,
  },
  infoHeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  infoText: {
    fontSize: 16,
  },
  listItem: {
    elevation: 7,
    borderRadius: 10,
    width: deviceWidth - 20,
    backgroundColor: 'white',
    margin: 10,
  },
  listItemImage: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: '100%',
    height: 200,
  },
  listItemBody: {
    padding: 10,
  },
  listItemBodyStar: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  listItemText: {
    fontSize: 22,
    fontWeight: 'bold',
  },
});

export default styles;
