import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from '../styles/styles';

export default function({item}) {
  const navigation = useNavigation();
  const stars = [1, 2, 3, 4, 5];
  return (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => navigation.navigate('Детально гостиницы', {item: item})}>
      <Image source={item.images[0]} style={styles.listItemImage} />
      <View style={styles.listItemBodyStar}>
        <View style={{flex: 1}}>
          <Text style={styles.listItemText}>{item.name}</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          {stars.map(i => {
            let color = '#ebebeb';
            if (i <= item.rating) {
              color = '#F6D235';
            }
            return <Icon name="star" size={30} color={color} key={i} />;
          })}
        </View>
      </View>
    </TouchableOpacity>
  );
}
