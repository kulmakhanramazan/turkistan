import * as React from 'react';
import {View, Text} from 'react-native';

import styles from '../styles/styles';

export default function({title}) {
  return (
    <View style={[styles.header, styles.shadow]}>
      <Text style={styles.headerText}>{title}</Text>
    </View>
  );
}
