import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styles from '../styles/styles';

export default function({item}) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => navigation.navigate('Детально памятники', {item: item})}>
      <Image source={item.images[0]} style={styles.listItemImage} />
      <View style={styles.listItemBody}>
        <Text style={styles.listItemText}>{item.name}</Text>
      </View>
    </TouchableOpacity>
  );
}
