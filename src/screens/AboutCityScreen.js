import * as React from 'react';
import {LocalizationContext} from '../locales/Locale';
import {Image, ScrollView, Text, View} from 'react-native';
import styles from '../styles/styles';
import I18n from '../locales/I18n';
import Header from '../components/Header';

const AboutCityScreen = () => {
  const {t} = React.useContext(LocalizationContext);
  return (
    <ScrollView style={[styles.content, {backgroundColor: 'white'}]}>
      <Header title={t('information')} />
      <View style={styles.infoTitleContainer}>
        <Image
          source={require('../assets/images/ahmet.png')}
          style={styles.infoImage}
        />
        <View style={styles.infoTitleWrapper}>
          <Text style={styles.infoTitle}>{t('turkestan')}</Text>
          <Text style={styles.infoSubtitle}>{t('titleTurkestan')}</Text>
        </View>
      </View>
      <View style={styles.infoBody}>
        <View style={styles.infoHeader}>
          <Text style={styles.infoHeaderText}>{t('history')}</Text>
        </View>
        <Text style={styles.infoText}>{t('historyText')}</Text>
        <View style={styles.infoHeader}>
          <Text style={styles.infoHeaderText}>{t('weather')}</Text>
        </View>
        <Text style={styles.infoText}>{t('weatherText')}</Text>
        <View style={styles.infoHeader}>
          <Text style={styles.infoHeaderText}>{t('ethno')}</Text>
        </View>
        <Text style={styles.infoText}>{t('ethnoText')}</Text>
        <View style={styles.infoHeader}>
          <Text style={styles.infoHeaderText}>{I18n.t('administrative')}</Text>
        </View>
        <Text style={styles.infoText}>{t('administrativeText')}</Text>
        <View style={styles.infoHeader}>
          <Text style={styles.infoHeaderText}>{t('education')}</Text>
        </View>
        <Text style={styles.infoText}>{t('educationText')}</Text>
      </View>
    </ScrollView>
  );
};

export default AboutCityScreen;
