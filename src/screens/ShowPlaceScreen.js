import React from 'react';
import {View, ScrollView} from 'react-native';

import Header from '../components/Header';
import styles from '../styles/styles';
import {ShowPlaceDataRu} from '../data/ShowPlaceData.ru';
import {ShowPlaceDataKk} from '../data/ShowPlaceData.kk';
import ShowPlaceItem from '../components/ShowPlaceItem';
import {LocalizationContext} from '../locales/Locale';
import {ShowPlaceDataEn} from '../data/ShowPlaceData.en';

export default function() {
  let data = [];

  const {t, locale} = React.useContext(LocalizationContext);

  if (locale === 'ru') {
    data = ShowPlaceDataRu;
  } else if (locale === 'kk'){
    data = ShowPlaceDataKk;
  } else {
    data = ShowPlaceDataEn;
  }

  return (
    <View style={styles.container}>
      <Header title={t('showPlace')} />
      <ScrollView style={[styles.content]}>
        {data.map(item => {
          return <ShowPlaceItem key={item.id} item={item} />;
        })}
      </ScrollView>
    </View>
  );
}
