import React from 'react';
import {View, ScrollView} from 'react-native';

import Header from '../components/Header';
import styles from '../styles/styles';
import {HOTELS_DATA_RU} from '../data/HotelsData.ru';
import {HOTELS_DATA_KK} from '../data/HotelsData.kk';
import HotelItem from '../components/HotelItem';
import {LocalizationContext} from '../locales/Locale';
import {HOTELS_DATA_EN} from '../data/HotelsData.en';

export default function() {
  const {t, locale, setLocale} = React.useContext(LocalizationContext);
  let data = [];

  if (locale === 'ru') {
    data = HOTELS_DATA_RU;
  } else if(locale === 'kk') {
    data = HOTELS_DATA_KK;
  } else {
    data = HOTELS_DATA_EN;
  }

  return (
    <View style={styles.container}>
      <Header title={t('hotels')} />
      <ScrollView style={[styles.content]}>
        {data.map(item => {
          return <HotelItem key={item.id} item={item} />;
        })}
      </ScrollView>
    </View>
  );
}
