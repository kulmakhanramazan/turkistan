import React from 'react';
import {ScrollView, View, Text, Switch} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../components/Header';
import {deviceHeight} from '../styles/styles';
import {LocalizationContext} from '../locales/Locale';

export default function() {
  const {t, locale, setLocale} = React.useContext(LocalizationContext);

  const setLang = code => {
    AsyncStorage.setItem('@locale_Key', code).then(() => {
      setLocale(code);
    });
  };

  return (
    <ScrollView style={{height: deviceHeight, backgroundColor: 'white'}}>
      <Header title={t('settings')} />
      <Text
        style={{
          color: 'gray',
          fontSize: 14,
          paddingHorizontal: 15,
          marginVertical: 10,
        }}>
        {t('choose')}
      </Text>
      <View
        style={{
          paddingHorizontal: 15,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text onPress={() => setLang('ru')} style={{fontSize: 20}}>
          Русский
        </Text>
        <Switch
          onChange={() => setLang('ru')}
          value={locale === 'ru'}
          disabled={false}
        />
      </View>
      <View
        style={{
          paddingHorizontal: 15,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text onPress={() => setLang('kk')} style={{fontSize: 20}}>
          Қазақша
        </Text>
        <Switch
          onChange={() => setLang('kk')}
          value={locale === 'kk'}
          disabled={false}
        />
      </View>
      <View
        style={{
          paddingHorizontal: 15,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text onPress={() => setLang('en')} style={{fontSize: 20}}>
          English
        </Text>
        <Switch
          onChange={() => setLang('en')}
          value={locale === 'en'}
          disabled={false}
        />
      </View>
    </ScrollView>
  );
}
