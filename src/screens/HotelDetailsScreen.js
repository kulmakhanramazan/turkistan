import * as React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import styles, {deviceWidth, successColor} from '../styles/styles';
import I18n from '../locales/I18n';
import {LocalizationContext} from '../locales/Locale';

export default function({route, navigation}) {
  const {t, locale, setLocale} = React.useContext(LocalizationContext);

  const {item} = route.params;
  navigation.setOptions({
    title: item.name,
  });
  const [currentSlide, setCurrentSlide] = React.useState(0);
  return (
    <ScrollView style={styles.detailsContainer}>
      <View style={styles.detailsNameContainer}>
        <Carousel
          layout={'default'}
          data={item.images}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          renderItem={({item, index}) => renderItem(item, index)}
          onSnapToItem={index => setCurrentSlide(index)}
        />
        <CarouselPagination
          carouselLength={item.images.length}
          activeSlide={currentSlide}
        />
        <View style={styles.carouselTextBody}>
          <View
            style={{
              borderBottomWidth: 1,
              marginTop: 20,
              borderBottomColor: successColor,
            }}
          />
          <TouchableOpacity
            onPress={() => openUrl(item.link)}
            style={{
              backgroundColor: successColor,
              paddingHorizontal: 20,
              paddingVertical: 10,
              marginVertical: 20,
              borderRadius: 25,
            }}>
            <Text style={{fontSize: 22, color: 'white', textAlign: 'center'}}>
              {t('booking')}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: successColor,
            }}
          />
          <Text style={{fontSize: 16, marginVertical: 20}}>
            {item.description}
          </Text>
          <View
            style={{borderBottomWidth: 1, borderBottomColor: successColor}}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="pets" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>{t('pets')}</Text>
          </View>
          <Text style={{fontSize: 16}}>{t('petsText')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="restaurant" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>{t('eat')}</Text>
          </View>
          <Text style={{fontSize: 16}}>{t('eatText')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="room-service" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>
              {t('service')}
            </Text>
          </View>
          <Text style={{fontSize: 16}}>{t('service01')}</Text>
          <Text style={{fontSize: 16}}>{t('service02')}</Text>
          <Text style={{fontSize: 16}}>{t('service03')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="local-parking" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>
              {t('parking')}
            </Text>
          </View>
          <Text style={{fontSize: 16}}>{t('parkingText')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="wifi" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>{t('internet')}</Text>
          </View>
          <Text style={{fontSize: 16}}>{t('internetText')}</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="info" size={25} color={successColor} />
            <Text style={{fontSize: 25, marginLeft: 10}}>
              {t('common')}
            </Text>
          </View>
          <Text style={{fontSize: 16}}>{t('common01')}</Text>
          <Text style={{fontSize: 16}}>{t('common02')}</Text>
        </View>
      </View>
    </ScrollView>
  );
}

function renderItem(item, index) {
  return (
    <View style={styles.carouselItem}>
      <Image source={item} style={styles.carouselImage} />
    </View>
  );
}

function CarouselPagination({carouselLength, activeSlide}) {
  return (
    <Pagination
      dotsLength={carouselLength}
      activeDotIndex={activeSlide}
      containerStyle={{backgroundColor: 'white', paddingVertical: 8}}
      dotStyle={{
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        marginVertical: 0,
        paddingVertical: 0,
        backgroundColor: '#2AABE2',
      }}
      inactiveDotStyle={{
        backgroundColor: 'gray',
      }}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    />
  );
}

function openUrl(url) {
  Linking.openURL(url).then(() => console.log('Url: ', url));
}
