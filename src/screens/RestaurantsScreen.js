import React from 'react';
import {View, ScrollView} from 'react-native';

import Header from '../components/Header';
import styles from '../styles/styles';
import HotelItem from '../components/HotelItem';
import {RESTAURANTS_DATA_RU} from '../data/RestaurantsData.ru';
import {RESTAURANTS_DATA_KK} from '../data/RestaurantsData.kk';
import {LocalizationContext} from '../locales/Locale';
import {RESTAURANTS_DATA_EN} from '../data/RestaurantsData.en';

export default function() {
  const {t, locale} = React.useContext(LocalizationContext);

  let data = [];

  if (locale === 'ru') {
    data = RESTAURANTS_DATA_RU;
  } else if (locale === 'kk') {
    data = RESTAURANTS_DATA_KK;
  } else {
    data = RESTAURANTS_DATA_EN;
  }

  return (
    <View style={styles.container}>
      <Header title={t('restaurant')} />
      <ScrollView style={[styles.content]}>
        {data.map(item => {
          return <HotelItem key={item.id} item={item} />;
        })}
      </ScrollView>
    </View>
  );
}
