import * as React from 'react';
import {View, Text, ScrollView, Image, Linking} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

import styles, {deviceWidth, successColor} from '../styles/styles';
import {LocalizationContext} from '../locales/Locale';

export default function({route, navigation}) {
  const {item} = route.params;
  const {t} = React.useContext(LocalizationContext);
  navigation.setOptions({
    title: item.name,
  });
  const [currentSlide, setCurrentSlide] = React.useState(0);
  return (
    <ScrollView style={styles.detailsContainer}>
      <View style={styles.detailsNameContainer}>
        <Carousel
          layout={'default'}
          data={item.images}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          renderItem={({item, index}) => renderItem(item, index)}
          onSnapToItem={index => setCurrentSlide(index)}
        />
        <CarouselPagination
          carouselLength={item.images.length}
          activeSlide={currentSlide}
        />
        <View style={styles.carouselTextBody}>
          <View
            style={{
              borderBottomWidth: 1,
              marginVertical: 20,
              borderBottomColor: successColor,
            }}
          />
          <Text style={{fontSize: 18}}>{item.description}</Text>
          <View
            style={{
              borderBottomWidth: 1,
              marginVertical: 20,
              borderBottomColor: successColor,
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="phone" size={25} color={successColor} />
            <Text style={{fontSize: 20, marginLeft: 10}}>{item.phone}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="map" size={25} color={successColor} />
            <Text style={{fontSize: 20, marginLeft: 10}}>{item.address}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <MaterialIcon name="room-service" size={25} color={successColor} />
            <Text style={{fontSize: 20, marginLeft: 10}}>{item.type}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <FontAwesomeIcon name="money-bill" size={25} color={successColor} />
            <Text style={{fontSize: 20, marginLeft: 10}}>
              {item.price.min} - {item.price.max} {t('wallet')}
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

function renderItem(item, index) {
  return (
    <View style={styles.carouselItem}>
      <Image source={item} style={styles.carouselImage} />
    </View>
  );
}

function CarouselPagination({carouselLength, activeSlide}) {
  return (
    <Pagination
      dotsLength={carouselLength}
      activeDotIndex={activeSlide}
      containerStyle={{backgroundColor: 'white', paddingVertical: 8}}
      dotStyle={{
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        marginVertical: 0,
        paddingVertical: 0,
        backgroundColor: '#2AABE2',
      }}
      inactiveDotStyle={{
        backgroundColor: 'gray',
      }}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    />
  );
}

function openUrl(url) {
  Linking.openURL(url).then(() => console.log('Url: ', url));
}
