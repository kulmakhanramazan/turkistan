import * as React from 'react';
import {View, Text, ScrollView, Image} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import styles, {deviceWidth} from '../styles/styles';
import {ShowPlaceDataRu} from '../data/ShowPlaceData.ru';

export default function({route, navigation}) {
  const {item} = route.params;
  navigation.setOptions({
    title: item.name,
  });
  const [currentSlide, setCurrentSlide] = React.useState(0);
  return (
    <ScrollView style={styles.detailsContainer}>
      <View style={styles.detailsNameContainer}>
        <Carousel
          layout={'default'}
          data={item.images}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          renderItem={({item, index}) => renderItem(item, index)}
          onSnapToItem={index => setCurrentSlide(index)}
        />
        <CarouselPagination
          carouselLength={item.images.length}
          activeSlide={currentSlide}
        />
        <View style={styles.carouselTextBody}>
          {item.texts.map((i, index) => {
            return (
              <View key={index.toString()}>
                <View style={styles.infoHeader}>
                  <Text style={styles.infoHeaderText}>{i.header}</Text>
                </View>
                <Text style={styles.infoText}>{i.text}</Text>
              </View>
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
}

function renderItem(item, index) {
  return (
    <View style={styles.carouselItem}>
      <Image source={item} style={styles.carouselImage} />
    </View>
  );
}

function CarouselPagination({carouselLength, activeSlide}) {
  return (
    <Pagination
      dotsLength={carouselLength}
      activeDotIndex={activeSlide}
      containerStyle={{backgroundColor: 'white', paddingVertical: 8}}
      dotStyle={{
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        marginVertical: 0,
        paddingVertical: 0,
        backgroundColor: '#2AABE2',
      }}
      inactiveDotStyle={{
        backgroundColor: 'gray',
      }}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    />
  );
}
