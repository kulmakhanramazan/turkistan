export default {
  information: 'Information',
  showPlace: 'Monuments',
  hotels: 'Hotels',
  restaurant: 'Restaurants',
  settings: 'Settings',
  detailsRestaurant: 'Hotel Details',
  choose: 'Choose language',
  turkestan: 'Turkestan',
  titleTurkestan: 'Cultural Center of South Kazakhstan',
  history: 'Some history',
  historyText:
    'The city of Turkestan, located in the middle of the arid steppes of the extreme south of Kazakhstan, is a real gift in the history of the tourism industry. While neighboring Uzbekistan has a bunch of cities along the Great Silk Road with attractions of an appropriate scale and magnificence, Kazakhstan has remained aloof from trade routes and can’t boast of settlements with a long history. Turkestan is a happy exception.',
  weather: 'Weather in Turkestan',
  weatherText:
    'In Turkestan (and generally in the South Kazakhstan region), a temperate desert climate prevails with strong temperature fluctuations throughout the year. In the winter, it is mostly −5 or  −7 °C, but sometimes the temperature drops to −15 °C. In the summer, the thermometer confidently holds at around +30 °C, often crawling to +40 °C. The best time to visit Turkestan is from March to June and from September to November.',
  ethno: 'National composition',
  ethnoText:
    'National composition (at the beginning of 2019): Kazakh - 113 739 people (69.04%); Uzbek - 45 998 people (27.92%); Turk - 1 803 people (1.09%); Russian - 1 502 people (0.91%); Tatar - 700 people (0.42%); Azerbaijani - 362 people (0.22%); Tajik - 87 people (0.05%); Kyrgyz - 82 people (0.05%); Uyghur - 55 people (0.03%); Korean - 36 people (0.02%); Greek - 27 people (0.02%) and others - 379 people (0.23%). Total population - 164 746.',
  administrative: 'Administrative division',
  administrativeText:
    'Turkestan is surrounded by rural districts subordinate to the Kentau City Administration. The territory directly subordinated to the akimat of the city is 19 627 ha (196.27 km²), of this territory 9.8 thousand ha (98 km²) are built up; the total development area of the city in the future is 25 thousand hectares (250 km²) for a population of 1 million people. Akim of the city - Rashid Ayupov.',
  education: 'Education',
  educationText:
    'In 1991, Turkestan University was opened in the city with 13 faculties, in 1993 it was renamed the International Kazakh-Turkish University named after Khoja Ahmet Yassawi for 22 thousand students is considered the largest university in Central Asia by the number of students',
  booking: 'Book Now',
  pets: 'Pets',
  petsText: 'Pets are not allowed.',
  eat: 'Food & Beverage',
  eatText: 'Restaurants',
  service: 'Services',
  service01: 'Airport shuttle',
  service02: '24 hour reception',
  service03: 'Room service',
  parking: 'Parking',
  parkingText: 'Free private parking is available (reservation is not needed).',
  internet: 'Internet',
  internetText: 'Wi-Fi is available in the entire hotel for free.',
  common: 'General Information',
  common01: 'Air conditioning',
  common02: 'Non smoking rooms',
  university: 'Akhmet Yassawi University',
  aboutUniversity: 'About University',
  historyUniversity: 'History of university',
  textHistoryUniversity:
    'Akhmet Yassawi International Kazakh-Turkish University (Akhmet Yassawi University) established in 1991 on the personal initiative of the Head of State N.A. Nazarbayev and based on the Intergovernmental Agreement between Kazakhstan and Turkey to train modern highly qualified specialists from young Turkic-speaking countries, the spiritual center of the Turkic world – Turkestan and is the first university that received the status of an international institution of higher education.\n' +
    '\n' +
    'On October 31, 1992, the Governments of the Republic of Kazakhstan and the Republic of Turkey signed the Agreement on the reorganization of the university into Khoja Akhmet Yassawi International Kazakh-Turkish University.\n' +
    '\n' +
    'In January 2012, the Head of State signed the Law of the Republic of Kazakhstan on the ratification of the Agreement between the Government of the Republic of Kazakhstan and the Government of the Republic of Turkey on the operating conditions of the Khoja Akhmet Yassawi International Kazakh-Turkish University.',
  control: 'Management structure',
  controlText:
    'The activities of the Khoja Akhmet Yassawi International Kazakh-Turkish University are carried out in accordance with the Charter of the University and in the framework of the ratified agreement between the Government of the Republic of Kazakhstan and the Government of the Republic of Turkey on the conditions for the activities of the university. University management bodies are:\n' +
    '\n' +
    'Intergovernmental Board of Trustees\n' +
    'Senate\n' +
    'Management Board\n' +
    'Academic Committee\n' +
    'Scientific Committee\n' +
    'Strategic Planning and Quality Committee\n' +
    'Committee on educational issues',
  academicCommittee: 'Educational-methodical Council',
  academicCommitteeText:
    'Educational-methodical Council (EMC)-is a center that organizes methodical function of the university. EMC makes suggestions on direction and mechanism of managing the quality of higher and professional teaching.\n' +
    '\n' +
    'Main directions of the function of the EMC of the university:\n' +
    'examining the educational plans and working programs of specialties;\n' +
    'preparing textbooks, methodical manuals, and electronic textbooks ;\n' +
    '\n' +
    'Deals with implementing the result of education and research works into educational process. Makes suggestions on improvement of training specialists, analyzes, educational process if it is necessary examines educational-methodical documentations and makes decisions on suggesting given projects in documents. With the aim of implementing new technology, active methods of teaching and controlling the learning quality of the students organizes seminars, round tables and etc.\n' +
    'Prepares suggestions on approving module catalogues, improving the qualification of pedagogical and research personal and attesting system, analyzing the content of educational process.',
  aboutCity: 'About city',
  wallet: 'tg',
};
