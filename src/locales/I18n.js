import I18n from 'react-native-i18n';

import ru from './ru';
import kk from './kk';
import en from './en';

I18n.fallbacks = true;

I18n.translations = {
  ru,
  kk,
  en,
};

I18n.defaultLocale = 'kk';
export default I18n;
