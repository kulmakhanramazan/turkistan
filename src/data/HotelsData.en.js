import React from 'react';

export const HOTELS_DATA_EN = [
  {
    id: 1,
    name: 'Hotel Khanaka',
    description:
      'Khanaka Hotel is located in Turkistan in a building built in 2016. It features a bar and rooms with free WiFi. It features a European restaurant and free private parking. All rooms are equipped with a flat-screen TV. Rooms at Khanaka Hotel are air-conditioned and have a wardrobe. A continental and buffet breakfast is served each morning. The 24-hour reception staff speaks English, Russian and Turkish.',
    link: 'http://www.booking.com/Share-OiSUwKw',
    rating: 4,
    images: [
      require('../assets/images/hanaka01.jpg'),
      require('../assets/images/hanaka02.jpg'),
      require('../assets/images/hanaka03.jpg'),
    ],
  },
  {
    id: 2,
    name: 'EDEM Hotel',
    description:
      'Hotel EDEM has a bar and terrace. It features a restaurant, a 24-hour front desk and free WiFi. Room service is available. It offers free private parking and a paid airport shuttle service.\n' +
      'The air-conditioned rooms are equipped with a flat-screen cable TV, kettle, shower, hairdryer and desk. Guest rooms include a wardrobe and a private bathroom. A buffet breakfast is served each morning at EDEM. Hiking and other outdoor activities are popular in the vicinity of Turkistan.\n',
    link: 'http://www.booking.com/Share-7bMG7V',
    rating: 5,
    images: [
      require('../assets/images/edem01.jpg'),
      require('../assets/images/edem02.jpg'),
      require('../assets/images/edem02.jpg'),
    ],
  },
  {
    id: 3,
    name: 'Boutique Hotel Silk Way',
    description:
      'Silk Way Boutique Hotel in Turkistan offers a bar, garden, and terrace. It features a restaurant, a 24-hour front desk and free WiFi. Room service is available. Private parking is available at a surcharge. All rooms are air-conditioned and include a flat-screen satellite TV, kettle, bidet, hairdryer, and desk. Each room has a wardrobe and a private bathroom. Silk Way Boutique Hotel serves an à la carte breakfast daily.',
    link: 'http://www.booking.com/Share-kVfskS',
    rating: 4,
    images: [
      require('../assets/images/silk01.jpg'),
      require('../assets/images/silk02.jpg'),
      require('../assets/images/silk03.jpg'),
      require('../assets/images/silk04.jpg'),
    ],
  },
  {
    id: 4,
    name: 'EURASIA',
    description:
      'Hotel EURASIA is located in Turkestan. It features a restaurant, a 24-hour front desk and free WiFi throughout. Room service is available.\n' +
      'Private parking is available at an additional cost.\n' +
      'All units include a seating area and a flat-screen TV. Air-conditioned rooms at EURASIA Hotel feature a wardrobe.  An English / Irish breakfast is served in the morning.\n',
    link: 'http://www.booking.com/Share-WszEpDj',
    rating: 4,
    images: [
      require('../assets/images/euro01.jpg'),
      require('../assets/images/euro02.jpg'),
      require('../assets/images/euro03.jpg'),
    ],
  },
];
