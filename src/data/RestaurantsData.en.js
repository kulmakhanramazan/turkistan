export const RESTAURANTS_DATA_EN = [
  {
    id: 1,
    name: 'Shanyrak',
    address: 'Behind botanic park, Turkestan, Kazakhstan',
    phone: '+7 702 433 6515',
    type: 'Asian, Central Asian',
    description:
      'Shanyrak is not just a regular karaoke bar where you can have fun! Here you can have a real karaoke party, celebrate your birthday and feel like a real star !!! Cozy atmosphere, nice music and good company! A karaoke bar with separate booths is convenient for celebrations for any occasion. "New York" is a karaoke restaurant where everything is created with love and for guests! At your service is a huge selection of songs (over 60,000 songs), 3 stylish and different in design VIP halls. Every Tuesday Student\'s Day - karaoke with a 50% discount to all students !!! "Shanyrak" is the atmosphere of the holiday!',
    rating: 5,
    price: {
      min: 1000,
      max: 6000,
    },
    images: [
      require('../assets/images/shan01.jpg'),
      require('../assets/images/shan02.jpg'),
    ],
  },
  {
    id: 2,
    name: 'Edem Restaurant',
    address: 'Kozhanov, 6A, Turkestan Kazakhstan',
    phone: '+7 708 654 48 92',
    type: 'Central Asian',
    description:
      "Edem Restaurant is one of the most favorite institutions of residents and guests of Turkestan. Exquisite author's cuisine, modern design and dynamic atmosphere make this place special. A cozy gastronomic place in the afternoon and in the evening Edem Restaurant turns into a trendy bar for active people who know a lot about midnight fun. The restaurant is divided into a lively bar, a cozy lounge area and an excellent restaurant. The restaurant’s summer terrace is a balcony, from which you can watch on weekdays the bustle and traffic on the street.",
    rating: 5,
    price: {
      min: 3000,
      max: 15000,
    },
    images: [
      require('../assets/images/edemre01.jpg'),
      require('../assets/images/edemre02.jpg'),
    ],
  },
  {
    id: 3,
    name: 'Kafe Daulet',
    address: 'Kozhanov, 20, Turkestan 161224, Kazakhstan',
    phone: '+7 705 568 53 57',
    type: 'Turkey',
    description:
      'In the warm season, you can enjoy a special atmosphere, sitting in one of the cozy arbors, twined with greenery. The summer terrace is designed for 90 seats and includes 15 gazebos. The summer terrace of the restaurant is exactly what you need for a good rest. To the sound of a huge waterfall you can feel the harmony with nature and feel the surge of positive vital energy.',
    rating: 4,
    price: {
      min: 2000,
      max: 8000,
    },
    images: [require('../assets/images/dau01.jpg')],
  },
  {
    id: 4,
    name: 'Antalya Cafe Restaurant',
    address: 'Sultanbek Kosanov, Turkestan, Kazakhstan',
    phone: '+7 702 481 5644',
    type: 'Turkey',
    description:
      'The total area of the Antalya Cafe Restaurant is 1.5 thousand square meters. In the hotel complex, equipped with modern equipment, there is a mini market, hairdresser, cafe, bathhouse and pharmacy.',
    rating: 4,
    price: {
      min: 1000,
      max: 9000,
    },
    images: [
      require('../assets/images/anta01.jpg'),
      require('../assets/images/anta02.jpg'),
    ],
  },
];
