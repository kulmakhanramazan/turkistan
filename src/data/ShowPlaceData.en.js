import React from 'react';

export const ShowPlaceDataEn = [
  {
    id: 1,
    name: 'Mausoleum of Khoja Ahmed Yasawi',
    texts: [
      {
        header: 'Location',
        text:
          'The Mausoleum of Khoja Ahmed Yesevi is situated in the north-eastern part of the modern-day town of Turkestan (formerly known as Hazrat-e Turkestan),[3][5] an ancient centre of caravan trade known earlier as Khazret and later as Yasi,[8] in the southern part of Kazakhstan. The structure is within the vicinity of a historic citadel,[9] which is now an archaeological site.[3]\n' +
          '\n' +
          'Remains of medieval structures such as other mausoleums, mosques and bath houses characterize the archaeological area.[3] To the north of the Mausoleum of Khawaja Ahmed Yasawi, a reconstructed section of the citadel wall from the 1970s separates the historical area from the developments of the modern town.',
      },
      {
        header: 'History',
        text:
          'Khoja Ahmed Yasawi (Khawaja or Khwaja (Persian: خواجه pronounced khâje) corresponds to "master", whence Arabic: خواجة khawājah), also spelled as Khawajah Akhmet Yassawi or Hoca Ahmed Yesevi, was the 12th-century head of a regional school of Sufism, a mystic movement in Islam which began in the 9th century.[3] He was born in Ispidjab (modern Sayram) in 1093, and spent most of his life in Yasi, dying there in 1166.[2] He is widely revered in Central Asia and the Turkic-speaking world for popularizing Sufism,[10] which sustained the diffusion of Islam in the area despite the contemporary onslaught of the Mongol invasion.[3] The theological school he created turned Yasi into the most important medieval enlightening center of the area.[11] He was also an outstanding poet, philosopher and statesman.[11] Yasawi was interred in a small mausoleum, which became a pilgrimage site for Muslims.',
      },
      {
        header: 'New mausoleum',
        text:
          "The town of Yasi was largely spared during the Mongol invasion of Khwarezmia in the 13th century.[12] Overtime, the descendants of the Mongols settled in the area and converted to Islam.[1] The town then came under the control of the Timurid Dynasty in the 1360s.[3] Timur (Tamerlane), the founder of the dynasty, expanded the empire's realm to include Mesopotamia, Iran, and all of Transoxiana, with its capital located in Samarkand.[3] To gain the support of local citizens, Timur adopted the policy of constructing monumental public and cult buildings.[12] In Yasi, he put his attention to the construction of a larger mausoleum to house Yasawi's remains,[13][14] with the intention of glorifying Islam, promoting its further dissemination, and improving the governance of the immediate areas",
      },
      {
        header: 'Decline and preservation',
        text:
          'When the Timurid Empire disintegrated, control of the immediate territory passed on to the Kazakh Khanate, which made Yasi, then renamed Turkestan, its capital in the 16th century.[6][16] The khans (Turkic for "ruler") sought to strengthen the political and religious importance of Turkestan to unify the nomadic tribes within the young state.[16] Hence, as the khanate\'s political center, ceremonies for the elevation of the khans to the throne and missions from neighboring states were received in Turkestan.[6] The Kazakh nobility also held their most important meetings to decide state-related matters in the capital.',
      },
    ],
    images: [
      require('../assets/images/ahmet01.jpg'),
      require('../assets/images/ahmet02.jpg'),
      require('../assets/images/ahmet05.jpg'),
      require('../assets/images/ahmet04.jpg'),
      require('../assets/images/ahmet03.jpg'),
      require('../assets/images/ahmet06.jpg'),
    ],
  },
  {
    id: 2,
    name: 'Arystan Bab Mausoleum',
    texts: [
      {
        header: 'History',
        text:
          "A legend states that Emir Timur ordered the construction of a mosque on the site of Khoja Akhmet Yassawi's grave but all attempts were unsuccessful. Timur was then told in a dream that in order to have success he should first build a mausoleum over grave of the mystic Arystan Baba.[1]\n" +
          '\n' +
          "The mausoleum dates from the 14th century and is constructed over Arystan Baba's 12th-century grave but was reconstructed several times up to the 18th century.[2] In the 18th century the previous mausoleum, which had been destroyed by an earthquake was replaced with a double domed structure supported by two carved wooden columns.[1] Most of the current structure was constructed in the first decade of the 20th century with only the carved wooden pillars remaining from the original building.",
      },
      {
        header: 'Description',
        text:
          'The mausoleum features a large central arch and wide front facade with minarets at the ends and two large domes to the left of the main arch. As well as the two-chambered table-tomb (gurkhana) of Arystan Bab and three of his students, Hermet-Azyra, Karga-Baba and Lashyn-Baba, a mosque and auxiliary quarters and museum are located in the other rooms of the mausoleum.[2][3] The effect of high groundwater levels led to the mosque being demolished and rebuilt in 1971.[4] A quran showing medieval calligraphy is displayed under glass here.[1]\n' +
          '\n' +
          'The mausoleum is today a place of pilgrimage.',
      },
      {
        header: 'Arystan Baba',
        text:
          'According to legends Arystan Baba, a religious mystic, was the recipient of Mohammad\'s amanat beads or persimmon stone. When he was in Sayram he then passed these on to the eleven-year-old Khodja Akhmed Yassaui who became his pupil.[5] The people recognized Arystan Baba as a Saint (Aulie). So every Thursday, pilgrims spend the night at his grave and say prayers. A popular expression is widely spread: "spend the Night at Arystan Baba, ask Khoja Ahmed".[6] Arystan Baba lived for over 400 years before his death',
      },
    ],
    images: [
      require('../assets/images/abab03.jpg'),
      require('../assets/images/abab02.jpg'),
      require('../assets/images/abab01.jpg'),
    ],
  },
  {
    id: 3,
    name: 'Ukash-Ata well',
    texts: [
      {
        header: 'Description',
        text:
          'Ukasha-Ata well is a unique place of Kazakhstan. The well is a narrow tunnel covered with stones, which goes to a depth of 25 meters at a slight slope. An ordinary bucket can hardly fit in this well. The water in the well goes underground, but not everyone can get water from the well. It is known that the water in the well Ukasha-Ata has healing properties. Many years ago, some lucky people raised not only water from the well, but also flowers, amulets, stones and more, which was considered a great achievement.',
      },
      {
        header: 'History',
        text:
          'Ukasha-Ata well has a very long and interesting history. It all starts with the fact that there are many legends about Ukasha-Ata, one of the best disciples of the Prophet Muhammad. Ukasha-Ata was a very brave warrior who had never lost a battle. But soon the soldier died. During the morning prayer, his enemies beheaded him with a sword. Then he fell headlong into a well and came from Mecca to the Prophet Muhammad, who immediately learned of the death of one of his disciples. In order not to remain at the bottom of the blood, this place was buried with the body in a mausoleum with a 21-meter sarcophagus, blood flowed from the body at a distance. Thus, the mausoleum became a place of pilgrimage. It is 4 meters high and 4.6 meters wide.',
      },
      {
        header: 'See a well',
        text:
          'The wonderful sacred well of Ukasha Ata is open to the public. Today, anyone can and can see. The mausoleum and the well have become a very real and spiritual place for many visitors. First you can go to the mausoleum, pray and ask for a blessing, and then you can go to the well itself. The popularity of the St. Ukash well is immediately noticeable - there are always a lot of people and long lines near it. Many tourists in standby mode get tents or tents, as the wait can take up to several hours. Despite the long wait, understanding and calm, there is no anger and humiliation, no one goes without a line, because everyone understands that everyone comes here with their problems and requests.',
      },
    ],
    images: [
      require('../assets/images/ukash03.jpg'),
      require('../assets/images/ukash02.jpg'),
      require('../assets/images/ukash01.jpg'),
    ],
  },
  {
    id: 4,
    name: 'Mausoleum of the Gauhar Ana',
    texts: [
      {
        header: 'Description',
        text:
          'Gaukhar-Ana is a monument to the daughter of the great poet and thinker Hodja Ahmed Yassavi, near the city of Turkestan. The mausoleum is part of the state historical and cultural reserve-museum "Azret-Sultan". Mother Diamond received a gift of a healer, thanks to which she helped many people. An interesting fact is that near the mausoleum there are wells with artesian healing water. There is a legend that there is an underground passage from the mausoleum of Hodja Ahmed Yasawi to the mausoleum of Gauhar-An. The daughter of the famous poet died while going to the underground culvert in order not to leave the master and not to be exposed to the sun.',
      },
    ],
    images: [
      require('../assets/images/gauhar.jpg'),
      require('../assets/images/gauhar02.jpg'),
    ],
  },
];
