import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import RestaurantsScreen from '../screens/RestaurantsScreen';
import RestaurantDetailsScreen from '../screens/RestaurantDetailsScreen';
const Stack = createStackNavigator();

export default function() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Рестораны"
        component={RestaurantsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Детально гостиницы"
        component={RestaurantDetailsScreen}
      />
    </Stack.Navigator>
  );
}
