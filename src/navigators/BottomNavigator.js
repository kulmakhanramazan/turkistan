import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {successColor} from '../styles/styles';
import ShowPlaceStack from './ShowPlaceStack';
import HotelStack from './HotelStack';
import RestaurantStack from './RestaurantStack';
import SettingsScreen from '../screens/SettingsScreen';
import {LocalizationContext} from '../locales/Locale';
import AboutCityScreen from '../screens/AboutCityScreen';

const Tab = createBottomTabNavigator();

export default function() {
  const {t} = React.useContext(LocalizationContext);

  return (
    <Tab.Navigator
      initialRouteName={t('information')}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === t('information')) {
            iconName = 'information';
          } else if (route.name === t('showPlace')) {
            iconName = 'castle';
          } else if (route.name === t('hotels')) {
            iconName = 'hotel';
          } else if (route.name === t('restaurant')) {
            iconName = 'food-variant';
          } else if (route.name === t('settings')) {
            iconName = 'settings';
          }
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: successColor,
        inactiveTintColor: 'gray',
        inactiveBackgroundColor: 'white',
        tabStyle: {
          backgroundColor: 'white',
        },
      }}>
      <Tab.Screen name={t('information')} component={AboutCityScreen} />
      <Tab.Screen name={t('showPlace')} component={ShowPlaceStack} />
      <Tab.Screen name={t('hotels')} component={HotelStack} />
      <Tab.Screen name={t('restaurant')} component={RestaurantStack} />
      <Tab.Screen name={t('settings')} component={SettingsScreen} />
    </Tab.Navigator>
  );
}
