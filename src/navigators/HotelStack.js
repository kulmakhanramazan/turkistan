import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import HotelsScreen from '../screens/HotelsScreen';
import HotelDetailsScreen from '../screens/HotelDetailsScreen';
import I18n from '../locales/I18n';

const Stack = createStackNavigator();

export default function() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={I18n.t('information')}
        component={HotelsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Детально гостиницы" component={HotelDetailsScreen} />
    </Stack.Navigator>
  );
}
