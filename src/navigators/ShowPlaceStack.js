import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ShowPlaceScreen from '../screens/ShowPlaceScreen';
import ShowPlaceDetailsScreen from '../screens/ShowPlaceDetailsScreen';
const Stack = createStackNavigator();

export default function() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Памятники"
        component={ShowPlaceScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Детально памятники"
        component={ShowPlaceDetailsScreen}
      />
    </Stack.Navigator>
  );
}
